var mysql = require('mysql');
// var config = {
// 	host : "localhost",
//     user : "root",
//     password: 'c0mpl1ance',
//     database: 'japaneselearning' 
// }

var config = {
	host : "localhost",
    user : "shaun",
    password: 'password',
    database: 'japaneselearning' 
}

module.exports = {
	startSQL: function(){
		connection = mysql.createConnection(config);
		return connection;
	},

	doQuery: function(statement,callback){
		connection.query(statement,function(err,result){
			if(err){
				callback(err,null);
				connection.end();
			}
			else{
				callback(null,result);
				connection.end();
			}
		});
	}

}

//if disconnect happens
function handleDisconnect(){
	connection = mysql.createConnection(config);

	connection.connect(function(err){
		if(err){
			console.log('error connecting:',err);
			setTimeout(handleDisconnect,5000);
		}
	});

	connection.on('error',function(err){
		console.log('database error',err);
		if(err.code === 'PROTOCOL_CONNECTION_LOST'){
			handleDisconnect();
		}
		else{
			throw err;
		}
	});

}
handleDisconnect();