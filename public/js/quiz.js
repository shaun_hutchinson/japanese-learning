var count = 0;
var totalQuestion = 0;
var correctAnswers = 0;
var shuffledEnglish = [];
var shuffledJapanese = [];
var currentWord = "";
var user = null;

//initial setup
function startup(amount,userInfo){
	correctAnswers = 0;
	count = 0;
	user = userInfo;
	date = formattedDate;
	quizYear = year;
	$('#resultDiv').fadeOut(1000);
	totalQuestion = amount;
	var numArray = Array.apply(null,{length: data.length}).map(Number.call,Number);
	//shuffles numbers around
	var random = _.sample(numArray,totalQuestion);

	for (var i = 0; i < random.length; i++) {
		//setting up the lists for quiz
		shuffledEnglish.push(data[random[i]].englishWord);
		shuffledJapanese.push(data[random[i]].japaneseWord);
	};
	//set current word
	currentWord = shuffledJapanese[count];
	$('#questionWord').text(shuffledEnglish[count]);

}

var socket = io();
$('#submitAnswer').submit(function(){
	$("#checkBtn").attr("disabled",true);
	//wait 2 seconds before reenabling
	setTimeout(function(){
		$('#checkBtn').removeAttr("disabled");
	},2000);



	//sending to server for storage of marks
	var response = $('#answer').val();
	socket.emit('answer',$('#answer').val());

	var answer = currentWord;
	response = String(response).toLowerCase();


	var check = Boolean(answer == response);

	//the quiz is still going
	count += 1;	
	
		//correct
		if(check == true){
			socket.emit('correctCheck',"Correct!");
			$('#answer').val('');
			$('#check').text("Correct!");			
			correctAnswers += 1;
			
			setTimeout(function(){
				$('#check').text('...');
				changeWord(count);
			},1000);  
		
		}
		//incorrect
		else{
			socket.emit('correctCheck',"Incorrect!");
			$('#answer').val('');
			$('#check').text("Incorrect!");
			
			setTimeout(function(){
				$('#check').text('...');
				changeWord(count);
			},1000);
		}	

	//quiz complete
	if (count == totalQuestion){
		//reset to 0


		//quiz div fades out
		setTimeout(function(){
			$('#quiz').fadeOut(1000,function(){
				socket.emit('finish',
					{
					correct: correctAnswers,
					questions: totalQuestion,
					user: user.username,
					id: user.id,
					date: date,
					year: quizYear
				});
			})
		},1000);
		

		shuffledEnglish = [];
		shuffledJapanese = [];
		$('#result').text("Your score: " + correctAnswers + "\\" + totalQuestion);

		setTimeout(function(){
			$('#resultDiv').fadeIn(1000);
			$('#prep').fadeIn(1000);
			$('#startButton').text("Restart Quiz");
		},2000);
	}
	return false;
});

function changeWord(count){
	if(count < totalQuestion){
		currentWord = shuffledJapanese[count];
		$('#questionWord').text(shuffledEnglish[count]);

	}
}