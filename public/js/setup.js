var socket = io();
function startQuiz(){
    var amount = Number($('#amountQuestions').val());
    if(amount >= 5 && amount <= 20){
        startup(amount,userInfo);
        $('#prep').fadeOut(1000);
        setTimeout(function(){
            $('#quiz').fadeIn(1000);
        },1000);
        $('#amountQuestions').val('');
    }
    else if(amount == null){
        $('#chooseAmount').text("Invalid Number. Please Select a number between 5 and 20.");
    } 
    else{
        $('#chooseAmount').text("Invalid Number. Please Select a number between 5 and 20.");
    }                  
}
//starts the quiz when you press enter in the input as well
function enterQuiz(event){
    if(event.keyCode == 13){
        startQuiz();
        return false;
    }
}