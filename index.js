//load stuff we need
var express = require('express');
//var app = require('express');
var http = require('http');
//var https = require('https');
var io = require('socket.io')(http);
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var fs = require('fs');
var app = express();
var favicon = require('serve-favicon');



//my own included nodejs stuff
var sockets = require('./sockets');
var sql = require('./sqlSetup');

var connection = null;
var query = null;
var user = null;
var userInfo = null;
var admin = 0;
var server_port = process.env.PORT || 80;

 var privateKey = fs.readFileSync('sslcert/server.key','utf8');
 var certificate = fs.readFileSync('sslcert/server.crt','utf8');
 var credentials = {key: privateKey, cert: certificate};

//set view engine to ejs
app.set('view engine','ejs');

app.use(express.static(__dirname + '/public'));
app.use(favicon(__dirname + '/public/favicon.ico') );
app.use(cookieParser());

//session stuff
app.use(session({
	secret: '1234567890',
	resave: false,
	saveUninitialized: true,
	cookie: {
		httpOnly: true,
		secure: true,
		maxAge: null
	}
}));


//send variables as json through POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var httpServer = http.createServer(app);
//var httpsServer = https.createServer(credentials,app);

io = require('socket.io').listen(httpServer);

httpServer.listen(server_port,function(){
	console.log('running on port ' + server_port);
	sockets.startSocket(io);
});

//launch page
app.get('/',function(req,res){	
	req.session.currentUser = "";
	res.render('pages/login');
});

app.post('/',function(req,res){	
	req.session.currentUser = "";
	res.render('pages/login');
});

//user has logged in
app.post('/index',function(req,res){
	user = req.body.user;
	//setting session data
	req.session.currentUser = user;
	console.log(req.session.currentUser);
	res.render('pages/index',{
		username: user
	});
});

//home page from another page
app.get('/index',function(req,res){
	res.render('pages/index',{
		username: user
	});
});

//about
app.get('/about',function(req,res){
	res.render('pages/about',{
		username: user
	});
});

app.get('/create',function(req,res){
	res.render('pages/create');
});

app.get('/profile',function(req,res){
	connection = sql.startSQL();
	if(admin == 1){
		query = "SELECT users.username, results.userId, results.date, results.year, results.score, results.questions FROM users INNER JOIN results ON users.id=results.userId;";
		sql.doQuery(query,function(err,data){
			if(typeof(data) == null){
				res.send("no data :(");
			}
			else{
				res.render('pages/admin',{
					username: user,
					data: data
				});
			}
		});
	}
	else{
		query = "SELECT * FROM results WHERE userId = '" + userInfo.id + "'";
		sql.doQuery(query,function(err,data){
			if(typeof(data) == null){
				res.send("no data");
			}
			else{
				response = true;
				res.render('pages/scores',{
					username: user,
					data: data
				});
			}
		});	
	}
});

//test page
app.post('/test',function(req,res){
	connection = sql.startSQL();
	var select = req.body.selectPicker;
	var yearNum = select.substring(select.length-2);
	
	//just for year 9 only to display properly
	if(yearNum.indexOf("r") == 0){
		yearNum = yearNum.substring(yearNum.length-1);
	}
	console.log(yearNum);

	query = 'SELECT * FROM ' + select.replace(' ', ''); 
	sql.doQuery(query,function(err,data){
		var sqlData = null;
		if(err){
			console.log("Error: ", err);
		}
		else{
			sqlData = data;
			finishRequest(sqlData,yearNum);
		}
		
	})
	//the page rendering
	var finishRequest = function(result,yearLevel){
		res.render('pages/test',{
			rows: result,
			levelNumber: yearLevel,
			username: user,
			userInfo: userInfo
		});
	};

});
//socket IO
io.on('connection',function(socket){
	socket.on('login',function(details){
		console.log(details.username + " - " + details.password);
		checkUser(details.username,details.password);
	});

	socket.on('createUser',function(details){
		console.log(details.username + " - " + details.password);
		checkExists(details.username,details.password);
		//insertUser(details.username,details.password);
	})
});

function checkExists(username,password){
	var check;
	connection = sql.startSQL();
	console.log("im here now :D");
	var checkQuery = "SELECT (username) FROM users WHERE username = '" + username + "';";
	console.log(checkQuery);
	connection.query(checkQuery,function(err,result){
		console.log(result);
		if (result.length == 0){
			insertUser(username,password);
			io.emit('userCheck',true);
		}
		else{
			io.emit('userCheck',false);
		}
		
	})
}


//password checking
function checkUser(username,password){
	var DBUsername;
	var DBPassword;
	connection = sql.startSQL();
	
	var checkQuery = "SELECT id,username,password,admin from users WHERE username = '" + username + "';";
	console.log(checkQuery);
	connection.query(checkQuery,function(err,result){
		if(result.length == 0){
			console.log("username does not exist");
			io.emit('userCheck',false);			
			connection.end();
		}
		else{
			io.emit('userCheck',true);
			//determines admin status
			admin = result[0].admin;
			userInfo = {
				id: result[0].id,
				username: result[0].username
			};
			console.log(userInfo);

			DBUsername =  result[0].username;
			DBPassword = result[0].password;
			bcrypt.compare(password,DBPassword,function(err,response){
				console.log(response + " the reponse");
				console.log("emitting");
				io.emit('passwordCheck',response);
			});
			connection.end();
		}	
	});
}


//adding new user to DB
function insertUser(username,password){
	connection = sql.startSQL();
	bcrypt.genSalt(10,function(err,salt){
		bcrypt.hash(password, salt, function(err,hash){
			console.log(username);
			console.log(hash);

			var passQuery = "INSERT INTO users (username,password) VALUES ('" + username + "', '" + hash + "')";
			console.log(passQuery + ": the query");
			connection.query(passQuery,function(err,result){
				if(err) throw err;
				console.log("hey i did it mum");
				connection.end();

			});
		});
	});
}