var userCount = 0;
var express = require('express');
var http = require('http');
//var https = require('https');
var io = require('socket.io')(http);

var quiz = require('./saveQuiz');





module.exports = {
	startSocket : function(io){

		io.on('connection',function(socket){
			userCount++;
			io.emit('userCount',userCount);
			console.log("A user connected");

			socket.on('answer',function(answer){
				console.log('answer: ' + answer);
			});

			socket.on('finish',function(data){
				console.log("Score: " + data.correct + "\\" + data.questions);
				console.log("User: " + data.user + " : " + data.id);
				quiz.addScore(data);
			});

			socket.on('requestOnline',function(userCount){
				console.log("Users Online: " + userCount);
			});

			socket.on('disconnect',function(){
				console.log("A user disconnected");
				userCount--;

				//for users nagivating pages so the count doesnt always go down and up fast
				setTimeout(function(){
					io.emit('userCount',userCount);
				},5000);
			});
		});
	},

	startEmit : function(io){
		io.on('connection',function(socket){
			socket.emit()
		});
	}

};